
var arrNumber = [];
document.getElementById("btnThemSo").onclick = function () {
  var numberEl = document.querySelector("#nhapSo");
  var number = numberEl.value;
  arrNumber.push(number);
  // reset input
  numberEl.value = "";
  var result = arrNumber;
  var sum = tongSoDuong(arrNumber);
  var count = demSoDuong(arrNumber);
  var minN = min(arrNumber)
  var minP = minPositive(arrNumber);
  var lastNumber = findLastEvenNumber(arrNumber)
  var newArr = compareNumbers(arrNumber)
  var checkSNT = timSNTDauTien(arrNumber)
  var countInterger = demSoNguyen(arrNumber)
  var ketQua = soSanh(arrNumber)
  document.querySelector("#resultArray").innerHTML = `<span class='text-danger'>${result}</span>`;
  document.querySelector('#mangBanDau').innerHTML = `Mảng ban đầu: <span class='text-danger'>${result}</span>`
  document.querySelector("#resultLogic").innerHTML = `Tổng số dương: <span class='text-danger'>${sum}</span>
    <p>các số dương: <span class='text-danger'>${count}</span></p>
    <p>Số nhỏ nhất trong mảng: <span class='text-danger'>${minN}</span></p>
    <p>Số dương nhỏ nhất trong mảng: <span class='text-danger'>${minP}</span></p>
    <p>Số chẵn cuối cùng trong mảng: <span class='text-danger'>${lastNumber}</span></p>
    <p>Sắp xếp tăng dần: <span class='text-danger'>${newArr}</span></p>
    <p>Số Nguyên tố đầu tiên: <span class='text-danger'>${checkSNT}</span></p>
    <p>Đếm số nguyên: <span class='text-danger'>${countInterger}</span></p>
    <p>So sánh số dương và số âm: <span class='text-danger'>${ketQua}</span></p>`;
};


//1. Tổng số dương trong mảng
function tongSoDuong(arrNumber) {
  var sum = 0;
  for (var i = 0; i < arrNumber.length; i++) {
    if (arrNumber[i] > 0) {
      sum += Number(arrNumber[i]);
    }
  }
  return sum;
}
//2. Đếm có bao nhiêu số dương trong mảng.
function demSoDuong(arrNumber) {
  var count = 0;
  for (var i = 0; i < arrNumber.length; i++) {
    if (arrNumber[i] > 0) {
      count += 1;
    }
  }
  return count;
}
//3. Tìm số nhỏ nhất trong mảng.
function min(arrNumber) {
    var min = arrNumber[0];
    for (var i = 0; i < arrNumber.length; i++) {
      if ( Number(arrNumber[i]) < min)  {
        min = arrNumber[i];
      }
    }
    return min;
  }
//4. Tìm số dương nhỏ nhất trong mảng.
function minPositive(arrNumber) {
    var newArray = []
    for (var i = 0; i < arrNumber.length; i++){
        if(arrNumber[i] > 0){
            newArray.push(arrNumber[i])
        }
    }
    // console.log('newArray: ', newArray);
    var minP = newArray[0]
    for(var j = 0; j < newArray.length; j ++){
        if(minP > newArray[i]){
            minP = newArray[i]
        }
    }
    return minP
}
// 5. Tìm số chẵn cuối cùng trong mảng. 
function findLastEvenNumber(arrNumber){
    var lastNumber = arrNumber[0]
    for(var i = 0; i < arrNumber.length; i++){
        if(arrNumber[i] % 2 === 0){
            lastNumber = arrNumber[i]
        }
    }
    return lastNumber
}

// 6. Đổi chỗ 2 giá trị trong mảng theo vị trí (Cho nhập vào 2 vị trí muốn đổi chỗ giá trị). 
function hoanDoi(arrNumber, i, j){
  let temp = arrNumber[i];
  arrNumber[i] = arrNumber[j];
  arrNumber[j] = temp;
}
document.getElementById('btnDoiCho').onclick = function(){
  var i = document .getElementById('viTri1').value;
  var j = document .getElementById('viTri2').value;
  hoanDoi(arrNumber, i, j)
  document.querySelector('#ketQuaDoiCho').innerHTML = `Sau khi đổi chỗ: <span class='text-danger'>${arrNumber}</span>`
}

//7. Sắp xếp mảng theo thứ tự tăng dần.
function compareNumbers(arrNumber){
  var newArr = arrNumber.sort();
  return newArr;
}

// 8. Tìm số nguyên tố đầu tiên trong mảng. Nếu mảng không có số nguyên tố thì trả về – 1.
function timSNTDauTien(arrNumber){
  for (var i = 0; i < arrNumber.length; i++) {
    var checkSNT = true;
    if (arrNumber[i] < 2) { 
      continue;
    }
    for (var j = 2; j <= Math.sqrt(arrNumber[i]); j++) {
      if (arrNumber[i] % j === 0) {
        checkSNT = false;
        break;
      }
    }
    if (checkSNT) {
      return arrNumber[i];
      break
    }
  }
  return 'Không có SNT trong mảng'; 
}
// 9. Nhập thêm 1 mảng số thực, tìm xem trong mảng có bao nhiêu số nguyên?
function demSoNguyen(arrNumber){
  var countInterger = 0;
  for(var i = 0; i < arrNumber.length; i++){
    if(Number.isInteger(arrNumber[i]*1))
      countInterger += 1
  }
  console.log('countInterger: ', countInterger);
  return countInterger
}

//10. So sánh số lượng số dương và số lượng số âm xem số nào nhiều hơn.
function soSanh(arrNumber){
  var demSoDuong = 0;
  var demSoAm = 0;
  var ketQua = '';
  for (var i = 0; i < arrNumber.length; i++){
    if(arrNumber[i] > 0){
      demSoDuong += 1
    }else{
      demSoAm += 1
    }
  }
  if(demSoAm < demSoDuong){
    ketQua = 'Số âm < Số dương'
  }else if(demSoAm > demSoDuong){
    ketQua = 'Số âm > Số dương'
  }else{
    ketQua = 'Số âm = Số dương'
  }
  return ketQua
}

